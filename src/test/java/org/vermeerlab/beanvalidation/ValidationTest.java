/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation;

import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Test;
import org.vermeerlab.beanvalidation.form.FormValidation;
import org.vermeerlab.beanvalidation.messageinterpolator.BeanValidationMessageInterpolator;
import org.vermeerlab.targets.ValidationTarget;
import org.vermeerlab.targets.form.Form;
import org.vermeerlab.targets.form.FormItem;
import org.vermeerlab.targets.form.label.LabelForm;
import org.vermeerlab.targets.form.label.LabelFormDirectLabel;
import org.vermeerlab.targets.form.label.LabelFormItem;
import org.vermeerlab.targets.form.nolabel.FormHasFormItemWithPropertyKey;
import org.vermeerlab.targets.form.nolabel.FormItemNoLabel;
import org.vermeerlab.targets.form.nolabel.FormItemWithPropertyKey;
import org.vermeerlab.targets.form.nolabel.FormNoLabel;
import org.vermeerlab.targets.form.nolabel.FormWithPropertyKey;

/**
 *
 * @author Yamashita,Takahiro
 */
public class ValidationTest {

    @Test
    public void lazyMessageConvert() {

        ValidationTarget target = ValidationTarget.of("noMessage", "customTypeValue", "withMessage");
        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<ValidationTarget>> results = validator.validate(target);

        System.out.println("<<<<<<<<< 遅延変換 >>>>>>>>>");
        for (ConstraintViolation<ValidationTarget> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsBlankWithFormValidation() {
        Form form = new Form();
        FormItem item = new FormItem("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<Form>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationを優先的に実行するので、ValueObjectのValidationは実装されません >>>");

        for (ConstraintViolation<Form> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemValidateValueObject() {
        Form form = new Form();
        FormItem item = new FormItem("1");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<Form>> results = validator.validate(form);

        System.out.println("<<< FormValidationはエラーにならないので、ValueObjectの検証が実行されます。ラベルは未使用。 >>>");

        for (ConstraintViolation<Form> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsBlankWithFormValidation_FormLabel() {
        LabelForm form = new LabelForm();
        LabelFormItem item = new LabelFormItem("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<LabelForm>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationのメッセージにラベルが使用されます（FormObjecrt内指定） >>>");

        for (ConstraintViolation<LabelForm> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();

        }

    }

    @Test
    public void formItemIsBlankWithFormValidation_DirectLabel() {
        LabelFormDirectLabel form = new LabelFormDirectLabel();
        LabelFormItem item = new LabelFormItem("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<LabelFormDirectLabel>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationのメッセージにラベルが使用されます（直接指定） >>>");

        for (ConstraintViolation<LabelFormDirectLabel> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsNotBlankWithValueValidation_FormLabel() {
        LabelForm form = new LabelForm();
        LabelFormItem item = new LabelFormItem("2");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<LabelForm>> results = validator.validate(form);

        System.out.println("<<< ValueValidationのメッセージにラベルが使用されます（FormObjecrt内指定） >>>");

        for (ConstraintViolation<LabelForm> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();

        }

    }

    @Test
    public void formItemIsNotBlankWithValueValidation_DirectLabel() {
        LabelFormDirectLabel form = new LabelFormDirectLabel();
        LabelFormItem item = new LabelFormItem("2");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<LabelFormDirectLabel>> results = validator.validate(form);

        System.out.println("<<< ValueValidationのメッセージにラベルが使用されます（直接指定） >>>");

        for (ConstraintViolation<LabelFormDirectLabel> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsNotBlankWithFormValidation_InterfaceLabel() {
        FormNoLabel form = new FormNoLabel();
        FormItemNoLabel item = new FormItemNoLabel("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<FormNoLabel>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationのメッセージにプロパティ値が使用されます（FormObjecrt内指定） >>>");

        for (ConstraintViolation<FormNoLabel> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsNotBlankWithFormValidation_InterfaceLabel_DirectSet() {
        FormWithPropertyKey form = new FormWithPropertyKey();
        FormItemNoLabel item = new FormItemNoLabel("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<FormWithPropertyKey>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationのメッセージにプロパティ値が使用されます（直接指定） >>>");

        for (ConstraintViolation<FormWithPropertyKey> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }

    @Test
    public void formItemIsNotBlankWithFormValidation_InterfaceLabel_FormHasLabel() {
        FormHasFormItemWithPropertyKey form = new FormHasFormItemWithPropertyKey();
        FormItemWithPropertyKey item = new FormItemWithPropertyKey("");
        form.setItem(item);

        Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

        Set<ConstraintViolation<FormHasFormItemWithPropertyKey>> results = validator.validate(form, FormValidation.class);

        System.out.println("<<< FormValidationのメッセージにプロパティ値が使用されます（ラベルキーを指定） >>>");

        for (ConstraintViolation<FormHasFormItemWithPropertyKey> result : results) {
            BeanValidationMessageInterpolator interpolator = BeanValidationMessageInterpolator.create();
            String convertedMessage = interpolator.toMessage(result);

            System.out.println("設定した値                               = " + result.getInvalidValue());
            System.out.println("検証時に変換されたメッセージ = " + result.getMessage());
            System.out.println("遅延変換したメッセージ           = " + convertedMessage);
            System.out.println();
        }

    }
}
