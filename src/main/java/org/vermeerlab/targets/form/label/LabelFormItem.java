/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.targets.form.label;

import javax.validation.Valid;
import org.vermeerlab.beanvalidation.form.FormObject;
import org.vermeerlab.beanvalidation.form.Label;
import org.vermeerlab.targets.form.ValueObject;

/**
 *
 * @author Yamashita,Takahiro
 */
@Label("Form Class Label")
public class LabelFormItem implements FormObject<String> {

    @Valid
    private final ValueObject formValue;

    public LabelFormItem(String value) {
        this.formValue = ValueObject.of(value);
    }

    public String value() {
        return this.formValue.getValue();
    }

    @Override
    public String getValidatationValue() {
        return this.value();
    }

    @Override
    public String toString() {
        return this.value();
    }

}
