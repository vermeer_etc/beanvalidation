/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.targets;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Size;
import lombok.Value;
import org.vermeerlab.beanvalidator.CustomType;

/**
 *
 * @author Yamashita,Takahiro
 */
@Value(staticConstructor = "of")
public class ValidationTarget {

    @Size(min = 11)
    private final String noMessage;

    @CustomType
    private final String customTypeValue;

    @AssertTrue(message = "{custom.assert}")
    protected boolean isValid() {
        return false;
    }

    @Size(min = 100, message = "{min}以上の文字を指定してください。")
    private final String withMessage;

}
