package org.vermeerlab.beanvalidation.form;

import java.util.Objects;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class FormNotBlankValidator implements ConstraintValidator<FormNotBlank, FormObject<String>> {

    @Override
    public void initialize(FormNotBlank constraintAnnotation) {

    }

    @Override
    public boolean isValid(FormObject<String> value, ConstraintValidatorContext context) {
        return Objects.equals(value.getValidatationValue(), "") == false;
    }
}
