/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.form;

/**
 * Presentationで使用するFormクラスのインターフェース.
 *
 * @param <ReturnType> BeanValidationに使用する型
 *
 * @author Yamashita,Takahiro
 */
public interface FormObject<ReturnType> {

    /**
     * Formを検証に使用する値を返却します.
     * <P>
     * BeanValidationで使用する値はプリミティブ型である必要があり、ValueObjectにはAnnotateできません。
     * したがって、本メソッドを使用して内部のプリミティブ値を返却します.
     *
     * @return BeanValidationに使用する値
     */
    public ReturnType getValidatationValue();
}
