/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.Collections;
import java.util.Locale;
import javax.validation.ConstraintViolation;
import org.hibernate.validator.internal.engine.MessageInterpolatorContext;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;

/**
 * BeanValidationの検証結果メッセージを変換するクラス.
 *
 * @author Yamashita,Takahiro
 */
public class BeanValidationMessageInterpolator {

    private final ResourceBundleLocatorFactory resourceBundleLocatorFactory;
    private final FormObjectLabelReader formObjectLabelReader;

    private BeanValidationMessageInterpolator(ResourceBundleLocatorFactory resourceBundleLocatorFactory, FormObjectLabelReader formObjectLabelReader) {
        this.resourceBundleLocatorFactory = resourceBundleLocatorFactory;
        this.formObjectLabelReader = formObjectLabelReader;
    }

    public static BeanValidationMessageInterpolator create() {
        return BeanValidationMessageInterpolator.of(null);
    }

    public static BeanValidationMessageInterpolator of(Locale locale) {
        Locale _locale = locale == null ? Locale.getDefault() : locale;
        ResourceBundleLocatorFactory resourceBundleLocatorFactory = ResourceBundleLocatorFactory.of(_locale);
        FormObjectLabelReader formObjectLabelReader = FormObjectLabelReader.of(_locale);
        return new BeanValidationMessageInterpolator(resourceBundleLocatorFactory, formObjectLabelReader);
    }

    public String toMessage(ConstraintViolation<?> constraintViolation) {

        ValidationLabel validationLabel = ValidationLabel.of(constraintViolation, this.formObjectLabelReader);
        ResourceBundleLocator resourceBundleLocator = this.resourceBundleLocatorFactory.create(validationLabel);
        ResourceBundleMessageInterpolator interpolator = new ResourceBundleMessageInterpolator(resourceBundleLocator);

        MessageInterpolatorContext context = new MessageInterpolatorContext(
                constraintViolation.getConstraintDescriptor(),
                constraintViolation.getInvalidValue(),
                constraintViolation.getRootBeanClass(),
                validationLabel.getMessageParameters(),
                Collections.emptyMap()
        );
        String message = interpolator.interpolate(constraintViolation.getMessageTemplate(), context);
        return message;
    }

}
