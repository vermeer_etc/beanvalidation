/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.validation.ConstraintViolation;
import org.vermeerlab.beanvalidation.form.FormObject;
import org.vermeerlab.beanvalidation.form.Label;

/**
 *
 * @author Yamashita,Takahiro
 */
public class LabelConverter {

    private final ConstraintViolation<?> constraintViolation;

    private final List<String> pathes;

    private final List<String> labels;

    private int deepIndex;

    private LabelConverter(ConstraintViolation<?> constraintViolation, List<String> paths) {
        this.constraintViolation = constraintViolation;
        this.pathes = paths;
        this.labels = new ArrayList<>();
        this.deepIndex = 0;
    }

    public static LabelConverter of(ConstraintViolation<?> constraintViolation) {
        List<String> pathes = Arrays.asList(constraintViolation.getPropertyPath().toString().split("\\."));
        return new LabelConverter(constraintViolation, pathes);
    }

    public String filter() {
        Class<?> clazz = this.constraintViolation.getRootBeanClass();
        this.recursiveFilter(clazz, this.pathes.get(deepIndex));

        Class<?> invalidClazz = constraintViolation.getInvalidValue().getClass();
        Label annotation = invalidClazz.getAnnotation(Label.class);
        if (annotation != null) {
            this.labels.add(annotation.value());
        }

        if (FormObject.class.isAssignableFrom(invalidClazz)) {
            this.labels.add(invalidClazz.getCanonicalName() + ".label");
        }

        String _label = this.labels.isEmpty() ? "" : this.labels.get(0);
        return _label;

    }

    void recursiveFilter(Class<?> clazz, String property) {

        try {
            Label classLabel = clazz.getAnnotation(Label.class);
            if (classLabel != null) {
                this.labels.add(classLabel.value());
            }

            if (FormObject.class.isAssignableFrom(clazz)) {
                this.labels.add(clazz.getCanonicalName() + ".label");
            }

            Field field = clazz.getDeclaredField(property);
            Label fieldLabel = field.getAnnotation(Label.class);
            if (fieldLabel != null) {
                this.labels.add(fieldLabel.value());
            }

            Class<?> nextClass = clazz.getDeclaredField(property).getType();

            if (deepIndex < this.pathes.size() - 1) {
                deepIndex++;
                this.recursiveFilter(nextClass, this.pathes.get(deepIndex));
            }
        } catch (NoSuchFieldException | SecurityException ex) {
        }

    }

}
