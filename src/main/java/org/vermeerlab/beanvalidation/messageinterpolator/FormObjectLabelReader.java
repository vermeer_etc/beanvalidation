/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * FormObjectの項目表示名を取得するクラスです.
 * <P>
 * Property fileから FormObjectのclasspath + ".label" をキーとして表示項目名を取得します.
 *
 * @author Yamashita,Takahiro
 */
public class FormObjectLabelReader {

    private final ResourceBundle resoureceBundle;

    private FormObjectLabelReader(ResourceBundle resourceBundle) {
        this.resoureceBundle = resourceBundle;
    }

    public static FormObjectLabelReader of(Locale locale) {
        ResourceBundle.Control control = ResourceBundle.Control.getNoFallbackControl(ResourceBundle.Control.FORMAT_DEFAULT);
        ResourceBundle resoureceBundle = ResourceBundle.getBundle("FormObjectLabel", locale, control);
        return new FormObjectLabelReader(resoureceBundle);
    }

    public String getLabel(String key) {
        return this.resoureceBundle.containsKey(key)
               ? this.resoureceBundle.getString(key)
               : key.endsWith(".label")
                 ? ""
                 : key;
    }

}
