/*
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *  Copyright © 2018 Yamashita,Takahiro
 */
package org.vermeerlab.beanvalidation.messageinterpolator;

import java.util.HashMap;
import java.util.Map;
import javax.validation.ConstraintViolation;

/**
 * 検証対象の表示に使用されているLabelを扱うクラス.
 *
 * @author Yamashita,Takahiro
 */
class ValidationLabel {

    private final String labelValue;

    private final FormObjectLabelReader formObjectLabelReader;

    private ValidationLabel(String labelValue, FormObjectLabelReader formObjectLabelReader) {
        this.labelValue = labelValue;
        this.formObjectLabelReader = formObjectLabelReader;
    }

    public static ValidationLabel of(ConstraintViolation<?> constraintViolation, FormObjectLabelReader formObjectLabelReader) {
        String _labelValue = LabelConverter.of(constraintViolation).filter();
        return new ValidationLabel(_labelValue, formObjectLabelReader);
    }

    public String getValue() {
        return this.formObjectLabelReader.getLabel(this.labelValue);
    }

    public Map<String, Object> getMessageParameters() {
        Map<String, Object> map = new HashMap<>();
        map.put("label", this.getValue());
        return map;
    }

}
